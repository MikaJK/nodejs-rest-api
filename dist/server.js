/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/start.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/controller/duckController.js":
/*!******************************************!*\
  !*** ./src/controller/duckController.js ***!
  \******************************************/
/*! exports provided: http_get_all, http_post, http_get, http_put, http_delete */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"http_get_all\", function() { return http_get_all; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"http_post\", function() { return http_post; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"http_get\", function() { return http_get; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"http_put\", function() { return http_put; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"http_delete\", function() { return http_delete; });\n/* harmony import */ var _repository_duckRepository__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../repository/duckRepository */ \"./src/repository/duckRepository.js\");\n\n\n\nfunction http_get_all(request, response) {\n  var ducks = _repository_duckRepository__WEBPACK_IMPORTED_MODULE_0__[\"get_ducks\"];\n\n  if (ducks !== undefined) {\n    response.status(200).send(ducks);\n  } else {\n    response.status(400).send({\n      messgae: 'Bad request'\n    });\n  }\n}\n;\nfunction http_post(request, response) {\n  var duck = request.body;\n  var created_duck = duck = Object(_repository_duckRepository__WEBPACK_IMPORTED_MODULE_0__[\"create_duck\"])(duck);\n\n  if (created_duck !== undefined) {\n    response.status(201).send(created_duck);\n  } else {\n    response.status(400).send({\n      messgae: 'Bad request'\n    });\n  }\n}\n;\nfunction http_get(request, response) {\n  var duck_id = request.params.duck_id;\n  var duck = Object(_repository_duckRepository__WEBPACK_IMPORTED_MODULE_0__[\"get_duck\"])(duck_id);\n\n  if (duck !== undefined) {\n    response.status(200).send(duck);\n  } else {\n    response.status(404).send({\n      messgae: 'Not found'\n    });\n  }\n}\n;\nfunction http_put(request, response) {\n  var duck_id = request.params.duck_id;\n  var duck = request.body;\n  var updated_duck = Object(_repository_duckRepository__WEBPACK_IMPORTED_MODULE_0__[\"update_duck\"])(duck_id, duck);\n\n  if (updated_duck !== undefined) {\n    response.status(200).send(updated_duck);\n  } else {\n    response.status(404).send({\n      messgae: 'Not found'\n    });\n  }\n}\n;\nfunction http_delete(request, response) {\n  var duck_id = request.params.duck_id;\n\n  if (Object(_repository_duckRepository__WEBPACK_IMPORTED_MODULE_0__[\"delete_duck\"])(duck_id)) {\n    response.status(204).send({\n      message: 'Deleted'\n    });\n  } else {\n    response.status(404).send({\n      messgae: 'Not found'\n    });\n  }\n}\n;\n\n//# sourceURL=webpack:///./src/controller/duckController.js?");

/***/ }),

/***/ "./src/repository/duckRepository.js":
/*!******************************************!*\
  !*** ./src/repository/duckRepository.js ***!
  \******************************************/
/*! exports provided: get_ducks, create_duck, get_duck, update_duck, delete_duck */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"get_ducks\", function() { return get_ducks; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"create_duck\", function() { return create_duck; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"get_duck\", function() { return get_duck; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"update_duck\", function() { return update_duck; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"delete_duck\", function() { return delete_duck; });\n/* harmony import */ var uuid_v4__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! uuid/v4 */ \"uuid/v4\");\n/* harmony import */ var uuid_v4__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(uuid_v4__WEBPACK_IMPORTED_MODULE_0__);\n\n\nfunction _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }\n\nfunction _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }\n\nfunction _nonIterableSpread() { throw new TypeError(\"Invalid attempt to spread non-iterable instance\"); }\n\nfunction _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === \"[object Arguments]\") return Array.from(iter); }\n\nfunction _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }\n\n\nvar ducks = [];\nfunction get_ducks() {\n  return json(ducks);\n}\n;\nfunction create_duck(duck) {\n  duck.id = uuid_v4__WEBPACK_IMPORTED_MODULE_0___default()();\n  ducks.push(duck);\n  return duck;\n}\n;\nfunction get_duck(duck_id) {\n  var duck_index = ducks.findIndex(function (_ref) {\n    var id = _ref.id;\n    return id === duck_id;\n  });\n\n  if (duck_index < 0) {\n    return undefined;\n  } else {\n    return ducks[duck_index];\n  }\n}\n;\nfunction update_duck(duck_id, duck) {\n  var duck_index = ducks.findIndex(function (_ref2) {\n    var id = _ref2.id;\n    return id === duck_id;\n  });\n\n  if (duck_index < 0) {\n    return undefined;\n  } else {\n    if (duck.id === undefined) {\n      duck.id = duck_id;\n    }\n\n    ducks = _toConsumableArray(ducks), _defineProperty({}, duck_index, duck);\n    return duck;\n  }\n}\n;\nfunction delete_duck(duck_id) {\n  var duck_index = ducks.findIndex(function (_ref4) {\n    var id = _ref4.id;\n    return id === duck_id;\n  });\n\n  if (duck_index < 0) {\n    return false;\n  } else {\n    ducks = ducks.filter(function (item) {\n      return item.id !== duck_id;\n    });\n    return true;\n  }\n}\n;\n\n//# sourceURL=webpack:///./src/repository/duckRepository.js?");

/***/ }),

/***/ "./src/router/duckRouter.js":
/*!**********************************!*\
  !*** ./src/router/duckRouter.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return route; });\n/* harmony import */ var _controller_duckController__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../controller/duckController */ \"./src/controller/duckController.js\");\n\n\n\nfunction route(app) {\n  app.route(\"/ducks\").get(_controller_duckController__WEBPACK_IMPORTED_MODULE_0__[\"http_get_all\"]).post(_controller_duckController__WEBPACK_IMPORTED_MODULE_0__[\"http_post\"]);\n  app.route(\"/ducks/:duck_id\").get(_controller_duckController__WEBPACK_IMPORTED_MODULE_0__[\"http_get\"]).put(_controller_duckController__WEBPACK_IMPORTED_MODULE_0__[\"http_put\"])[\"delete\"](_controller_duckController__WEBPACK_IMPORTED_MODULE_0__[\"http_delete\"]);\n}\n;\n\n//# sourceURL=webpack:///./src/router/duckRouter.js?");

/***/ }),

/***/ "./src/server.js":
/*!***********************!*\
  !*** ./src/server.js ***!
  \***********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _router_duckRouter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./router/duckRouter */ \"./src/router/duckRouter.js\");\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! express */ \"express\");\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var body_parser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! body-parser */ \"body-parser\");\n/* harmony import */ var body_parser__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(body_parser__WEBPACK_IMPORTED_MODULE_2__);\n\n\n\n\n\nvar app = express__WEBPACK_IMPORTED_MODULE_1___default()();\napp.use(body_parser__WEBPACK_IMPORTED_MODULE_2___default.a.urlencoded({\n  extended: true\n}));\napp.use(body_parser__WEBPACK_IMPORTED_MODULE_2___default.a.json());\napp.set(\"json spaces\", 2);\nObject(_router_duckRouter__WEBPACK_IMPORTED_MODULE_0__[\"default\"])(app);\n/* harmony default export */ __webpack_exports__[\"default\"] = (app);\n\n//# sourceURL=webpack:///./src/server.js?");

/***/ }),

/***/ "./src/start.js":
/*!**********************!*\
  !*** ./src/start.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _server__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./server */ \"./src/server.js\");\n\nvar port = process.env.PORT || 3000;\n_server__WEBPACK_IMPORTED_MODULE_0__[\"default\"].listen(port);\nconsole.log(\"Duck RESTful API server started on: \".concat(port));\n\n//# sourceURL=webpack:///./src/start.js?");

/***/ }),

/***/ "body-parser":
/*!******************************!*\
  !*** external "body-parser" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"body-parser\");\n\n//# sourceURL=webpack:///external_%22body-parser%22?");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"express\");\n\n//# sourceURL=webpack:///external_%22express%22?");

/***/ }),

/***/ "uuid/v4":
/*!**************************!*\
  !*** external "uuid/v4" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"uuid/v4\");\n\n//# sourceURL=webpack:///external_%22uuid/v4%22?");

/***/ })

/******/ });
import { get_duck, create_duck }  from '../duckRepository';
var duey = new Object();
duey.name = "Duey Duck";

describe('GET DUCK', () => {
    it('should get zero duck', () => {
        expect(get_duck('aaa-bbb-ccc')).toBe(undefined);
    });

    it('should get one duck', () => {
        var result = create_duck(duey);
        expect(get_duck(result.id)).not.toBeNull();
        expect(get_duck(result.id).id).toBe(result.id);
        expect(get_duck(result.id).name).toBe(duey.name);
    });
});
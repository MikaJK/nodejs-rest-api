"use strict";
import uuid from 'uuid/v4';
var ducks = [];

/**
 * Get all ducks.
 */
export function get_ducks() {
    return ducks;
};

/**
 * Create duck.
 * 
 * @param {} duck 
 */
export function create_duck(duck) {
    if (duck.name === undefined) {
        return undefined;
    }
    duck.id = uuid();
    ducks.push(duck);
    return duck;
};

/**
 * Get duck.
 * 
 * @param {*} duck_id 
 */
export function get_duck(duck_id) {
    var duck_index = ducks.findIndex(({ id }) => id === duck_id);
    if (duck_index < 0) {
        return undefined;
    } else {
        return ducks[duck_index];
    }
};

/**
 * Update duck.
 * 
 * @param {*} duck_id 
 * @param {*} duck 
 */
export function update_duck(duck_id, duck) {
    var duck_index = ducks.findIndex(({ id }) => id === duck_id);

    if (duck_index < 0) {
        return undefined;
    } else {
        if (duck.id === undefined) {
            duck.id = duck_id;
        } else {
            console.log(`ID: ${duck.id}`);
        }
        ducks = [...ducks], {[duck_index]: duck};
        return duck;
    }
};

/**
 * Delete duck.
 * 
 * @param {*} duck_id 
 */
export function delete_duck(duck_id) {
    var duck_index = ducks.findIndex(({ id }) => id === duck_id);

    if (duck_index < 0) {
        return false;
    } else {
        ducks = ducks.filter(item => item.id !== duck_id);
        return true;
    }
};
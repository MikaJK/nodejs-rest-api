"use strict";
import { http_get_all, http_post, http_get, http_put, http_delete } from '../controller/duckController'

export default function route(app)
{
    app.route("/ducks")
        .get(http_get_all)
        .post(http_post);


    app.route("/ducks/:duck_id")
        .get(http_get)
        .put(http_put)
        .delete(http_delete);
};
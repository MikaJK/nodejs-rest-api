"use strict";
import route from './router/duckRouter'
import express from 'express';
import bodyParser from 'body-parser';
var app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.set("json spaces", 2);

route(app);

export default app;
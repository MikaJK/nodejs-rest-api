"use strict";
import {get_ducks, create_duck, get_duck, update_duck, delete_duck} from '../repository/duckRepository'

export function http_get_all(request, response) {
    console.log(`HTTP GET ALL`);
    var ducks = get_ducks();

    if (ducks !== undefined) {
        response.status(200).send(ducks);
    } else {
        response.status(400).send({ messgae: 'Bad request' });
    }
};

export function http_post(request, response) {
    var duck = request.body;
        
    var created_duck = duck = create_duck(duck);
    
    if (created_duck !== undefined) {
        response.status(201).send(created_duck);
    } else {
        response.status(400).send({ messgae: 'Bad request' });
    }
};

export function http_get(request, response) {
    var duck_id = request.params.duck_id;

    var duck = get_duck(duck_id);

    if (duck !== undefined) {
        response.status(200).send(duck);
    } else {
        response.status(404).send({ messgae: 'Not found' });
    }
};

export function http_put(request, response) {
    var duck_id = request.params.duck_id;
    var duck = request.body;
    
    var updated_duck = update_duck(duck_id, duck);

    if (updated_duck !== undefined) {
        response.status(200).send(updated_duck);
    } else {
        response.status(404).send({ messgae: 'Not found' });
    }
};

export function http_delete(request, response) {
    var duck_id = request.params.duck_id;
    
    if (delete_duck(duck_id)) {
        response.status(204).send({ message: 'Deleted' });
    }else {
        response.status(404).send({ messgae: 'Not found'});
    }
};
import '@babel/polyfill';
import app  from '../../app';
import supertest from 'supertest';
const getTestRequest = supertest(app);
var duck = {
    name: "Duey Duck"
};
var id;

describe('GET', () => {
    it('should get zero duck', async done => {
        id = "aaa-bbb-ccc";
        await getTestRequest.get(`/ducks/${id}`)
            .then((response) => {
                expect(response.statusCode).toBe(404);
                done();
            });
    });

    it('should create a duck', async done => {
        await getTestRequest.post('/ducks').send(duck)
            .then((response) => {
                expect(response.statusCode).toBe(201);
                expect(response.body.id).not.toBeNull();
                expect(response.body.name).toBe(duck.name);
                id = response.body.id;
                done();
            });
    });

    it('should get one duck', async done => {
        await getTestRequest.get(`/ducks/${id}`)
            .then((response) => {
                expect(response.statusCode).toBe(200);
                expect(response.body.id).not.toBeNull();
                expect(response.body.name).toBe(duck.name);
                done();
            });
    });
});
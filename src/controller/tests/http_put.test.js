import '@babel/polyfill';
import app  from '../../app';
import supertest from 'supertest';
const putTestRequest = supertest(app);
var duck = {
    name: "Duey Duck"
};
var id;

describe('PUT', () => {
    it('should create a duck', async done => {
        await putTestRequest.post('/ducks').send(duck)
            .then((response) => {
                expect(response.statusCode).toBe(201);
                expect(response.body.id).not.toBeNull();
                expect(response.body.name).toBe(duck.name);
                id = response.body.id;
                done();
            });
    });

    it('should update duck', async done => {
        duck.id = id;
        duck.name = "Huey Duck"
        await putTestRequest.put(`/ducks/${id}`).send(duck)
            .then((response) => {
                expect(response.statusCode).toBe(200);
                expect(response.body.id).toBe(duck.id);
                expect(response.body.name).toBe(duck.name);
                done();
            });
    });

    it('should get error', async done => {
        id = "ccc-ddd-eee";
        await putTestRequest.put(`/ducks/${id}`).send(duck)
            .then((response) => {
                expect(response.statusCode).toBe(404);
                done();
            });
    });
});